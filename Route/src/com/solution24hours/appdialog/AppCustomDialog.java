package com.solution24hours.appdialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class AppCustomDialog {
	private AlertDialog.Builder appAlertDialogBuilder;
	
	public AppCustomDialog(Context context) {
				appAlertDialogBuilder = new AlertDialog.Builder(context);
		appAlertDialogBuilder.setCancelable(false);
		
	}

	public void setSingleMessage(String title, String message) {
		appAlertDialogBuilder.setTitle(title);
		appAlertDialogBuilder.setMessage(message);
		appAlertDialogBuilder.setPositiveButton("Ok",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int arg1) {
						dialog.dismiss();
					}
				});
		appAlertDialogBuilder.show();
	}
}

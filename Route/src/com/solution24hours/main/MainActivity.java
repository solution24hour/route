package com.solution24hours.main;

import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import com.solution24hours.route.R;

public class MainActivity extends ActivityGroup {
	private TabHost tabHost;
	private View view;
	private Integer[] unselected={R.drawable.map_icon_normal , R.drawable.post_icon_normal,R.drawable.alert_icon_normal,R.drawable.schedule_icon_normal, R.drawable.currentline_icon_normal};
	private Integer[] selected={R.drawable.map_icon_selected , R.drawable.post_icon_selected,R.drawable.alert_icon_selected,R.drawable.schedule_icon_selected, R.drawable.currentline_icon_selected};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		tabHost = (TabHost) findViewById(android.R.id.tabhost);
		tabHost.setup(this.getLocalActivityManager());
		
		// Map tab
		Intent intentMap = new Intent().setClass(this,MapActivity.class);
		view = LayoutInflater.from(this).inflate(R.layout.map_tab,tabHost.getTabWidget(), false);
		TabSpec tabSpecMap = tabHost.newTabSpec("Map").setIndicator(view).setContent(intentMap);
		
		
		// Poat Tab
		Intent intentPost = new Intent().setClass(this,PostActivity.class);
		view = LayoutInflater.from(this).inflate(R.layout.post_tab,tabHost.getTabWidget(), false);
		TabSpec tabSpecPost = tabHost.newTabSpec("Post").setIndicator(view).setContent(intentPost);
		
		
		// Alert Tab
		Intent intentAlert = new Intent().setClass(this,AlertActivity.class);
		view = LayoutInflater.from(this).inflate(R.layout.alert_tab,tabHost.getTabWidget(), false);
		TabSpec tabSpecAlert = tabHost.newTabSpec("Alert").setIndicator(view).setContent(intentAlert);
		
		
		// Schedule Tab
		Intent intentSchedule = new Intent().setClass(this,ScheduleActivity.class);
		view = LayoutInflater.from(this).inflate(R.layout.schedule_tab,tabHost.getTabWidget(), false);
		TabSpec tabSpecSchedule = tabHost.newTabSpec("Schedule").setIndicator(view).setContent(intentSchedule);
		
		
		// Current line Tab
		Intent intentcurrentline = new Intent().setClass(this,CurrentlineActivity.class);
		view = LayoutInflater.from(this).inflate(R.layout.currentline_tab,tabHost.getTabWidget(), false);
		TabSpec tabSpeccurrentline = tabHost.newTabSpec("currentline").setIndicator(view).setContent(intentcurrentline);
		
		
		tabHost.addTab(tabSpecMap);
		tabHost.addTab(tabSpecPost);
		tabHost.addTab(tabSpecAlert);
		tabHost.addTab(tabSpecSchedule);
		tabHost.addTab(tabSpeccurrentline);
		tabHost.setCurrentTab(0);
		setTabColor(tabHost);
		tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
			
			@Override
			public void onTabChanged(String tabId) {
				//int tab=tabHost.getCurrentTab();
				setTabColor(tabHost);
				//tabHost.getTabWidget().getChildAt(tab).setBackgroundColor(Color.BLACK);
			}
		});
		
	}
	
	public  void setTabColor(TabHost tabhost) {

        for (int i = 0; i < tabhost.getTabWidget().getChildCount(); i++) {
            tabhost.getTabWidget().getChildAt(i).setBackgroundResource(unselected[i]);// unselected
        }
        tabhost.getTabWidget().setCurrentTab(0);
        tabhost.getTabWidget().getChildAt(tabhost.getCurrentTab())
                .setBackgroundResource(selected[tabhost.getCurrentTab()]); // selected
                                                                        // //have
                                                                        // to
                                                                        // change
    }
}

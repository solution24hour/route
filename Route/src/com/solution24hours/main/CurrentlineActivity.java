package com.solution24hours.main;

import com.solution24hours.appdialog.AppCustomDialog;
import com.solution24hours.appinternet.InternetManagers;
import com.solution24hours.async_task.Points_RouteAsyncTask;
import com.solution24hours.route.R;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.ListView;

public class CurrentlineActivity extends Activity {

	private ListView route_pointListView;
	private Context context;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activiry_currentline);
		this.context=this;
		intiailizeViews();
		if(new InternetManagers(this).GetNetworInformation())
		{
			new Points_RouteAsyncTask(this, "31", "14",route_pointListView).execute();
		}else {
			new AppCustomDialog(context).setSingleMessage("Waring", "Please Check your Internet Connection");
		}
		
	}

	private void intiailizeViews() {
		route_pointListView=(ListView)findViewById(R.id.route_pointListView);
	}
}

package com.solution24hours.login;

import com.solution24hours.forgetpassword.ForgetpasswordActivity;
import com.solution24hours.main.MainActivity;
import com.solution24hours.route.R;
import com.solution24hours.webservice.WebService;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends Activity implements OnClickListener{
	private Context context;
	private EditText userNameEditText;
	private EditText passwordEditText;
	private Button loginButton;
	private Button forgetPasswordButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		this.context=this;
		intiailizeViews();
		new WebService().getPoints_Route("http://91.205.154.167/WebService/ApiService.asmx?op=Points_Route", "31", "14");
	}

	@Override
	public void onClick(View view) {
		if (view.getId() == R.id.forgetPasswordButton) {
			Intent intent = new Intent(context, ForgetpasswordActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		} else if (view.getId() == R.id.loginButton) {
			Intent intent = new Intent(context, MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}
	}
	
	private void intiailizeViews() {
		userNameEditText = (EditText) findViewById(R.id.userNameEditText);
		passwordEditText = (EditText) findViewById(R.id.passwordEditText);
		loginButton = (Button) findViewById(R.id.loginButton);
		loginButton.setOnClickListener(this);
		forgetPasswordButton = (Button) findViewById(R.id.forgetPasswordButton);
		forgetPasswordButton.setOnClickListener(this);
	}

	
}

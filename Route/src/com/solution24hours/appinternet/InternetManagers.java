/**
 * 
 */
package com.solution24hours.appinternet;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 ***Author*** Md abdul Gafur. Khulna University. 
 */

public class InternetManagers {
	private ConnectivityManager connectivity;
	private NetworkInfo netInfo;
	private Context context;

	public InternetManagers(Context context) {

		this.context = context;
	}

	public boolean GetNetworInformation() {
		connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		netInfo = connectivity.getActiveNetworkInfo();

		if (netInfo != null) {
			return TestConectionIsValid();

		} else {
			return false;
		}

	}

	private boolean TestConectionIsValid() {
		if (netInfo.isConnected()) {
			return true;
		} else {
			return false;
		}
	}
}

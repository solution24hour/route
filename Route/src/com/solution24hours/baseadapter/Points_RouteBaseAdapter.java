package com.solution24hours.baseadapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.util.FloatMath;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.solution24hours.dal.Points_Route;
import com.solution24hours.route.R;

public class Points_RouteBaseAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<Points_Route> points_RoutesList;
	private ListView route_pointListView;
	private LayoutInflater layoutInflater;
	private View view;
	private TextView point_timeTextView;
	private TextView point_nameTextView;
	private ImageView expend_ImageView;
	private LinearLayout moredataLinearLayout;
	private LinearLayout detailLinearLayout;
	private int expended=-1;
	private int detail=-1;
	
	private int NONE = 0;
	private int DRAG = 1;
	private int ZOOM = 2;
	private int mode = NONE;
	private float oldDist = 1f;
	private float dx = 0, dy = 0, x = 0, y = 0;
	
	public Points_RouteBaseAdapter(Context context,ArrayList<Points_Route> points_RoutesList,ListView route_pointListView) {
		this.context=context;
		this.points_RoutesList=points_RoutesList;
		this.route_pointListView=route_pointListView;
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return points_RoutesList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return points_RoutesList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public void setExpended(int position, int detailDate)
	{
		expended=position;
		detail=detailDate;
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		view = layoutInflater.inflate(R.layout.points_route_cell, null);
		point_timeTextView=(TextView)view.findViewById(R.id.point_timeTextView);
		point_nameTextView=(TextView)view.findViewById(R.id.point_nameTextView);
		expend_ImageView=(ImageView)view.findViewById(R.id.expend_closeImageView);
		moredataLinearLayout=(LinearLayout)view.findViewById(R.id.moredataLinearLayout);
		detailLinearLayout=(LinearLayout)view.findViewById(R.id.detailLinearLayout);
		if(position==expended)
		{
			if(moredataLinearLayout.getChildCount()>0)
				moredataLinearLayout.removeAllViews();
			
			expend_ImageView.setImageResource(R.drawable.expend_icon);
			View childView=layoutInflater.inflate(R.layout.points_route_detail, null);
			LinearLayout leftLinearLayout=(LinearLayout)childView.findViewById(R.id.leftLinearLayout);
			LinearLayout rightLinearLayout=(LinearLayout)childView.findViewById(R.id.rightLinearLayout);
			LinearLayout dragAreaLinearLayout=(LinearLayout)childView.findViewById(R.id.dragAreaLinearLayout);
		
			if (points_RoutesList.get(position).getAction_ID().equals("1")) {
				rightLinearLayout.setVisibility(View.VISIBLE);
				TextView rightNameTextView = (TextView) childView.findViewById(R.id.rightNameTextView);
				ImageView rightImageView=(ImageView)childView.findViewById(R.id.rightImageView);
				if((points_RoutesList.get(position).getLoad_OK().equals("1")) && ((points_RoutesList.get(position).getOffload_OK().equals("1")) || (points_RoutesList.get(position).getOffload_OK().equals("0"))))
				{
					rightImageView.setPadding(1, 1, 1, 1);
					rightImageView.setBackgroundColor(Color.GREEN);
				}else if((points_RoutesList.get(position).getLoad_OK().equals("-1")) || (points_RoutesList.get(position).getOffload_OK().equals("-1")) ){
					rightImageView.setPadding(1, 1, 1, 1);
					rightImageView.setBackgroundColor(Color.RED);
				}
				
				rightNameTextView.setText(points_RoutesList.get(position).getObj_Name());
				rightLinearLayout.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								setExpended(expended, 1);
							}
						});
				moveObject(rightLinearLayout);
			} else if((points_RoutesList.get(position).getAction_ID().equals("2")) && (points_RoutesList.get(position).getLoad_OK().equals("1")) &&(points_RoutesList.get(position).getOffload_OK().equals("1"))){
				leftLinearLayout.setVisibility(View.VISIBLE);
				TextView leftNameTextView = (TextView) childView.findViewById(R.id.leftNameTextView);
				leftNameTextView.setText(points_RoutesList.get(position).getObj_Name());
				ImageView leftImageView=(ImageView)childView.findViewById(R.id.leftImageView);
				if((points_RoutesList.get(position).getLoad_OK().equals("1")) && ((points_RoutesList.get(position).getOffload_OK().equals("1")) || (points_RoutesList.get(position).getOffload_OK().equals("0"))))
				{
					leftImageView.setPadding(1, 1, 1, 1);
					leftImageView.setBackgroundColor(Color.GREEN);
				}else if((points_RoutesList.get(position).getLoad_OK().equals("-1")) || (points_RoutesList.get(position).getOffload_OK().equals("-1")) ){
					leftImageView.setPadding(1, 1, 1, 1);
					leftImageView.setBackgroundColor(Color.RED);
				}
				leftLinearLayout.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						setExpended(expended, 1);
					}
				});
				moveObject(leftLinearLayout);
			}else if((points_RoutesList.get(position).getAction_ID().equals("2")) && (points_RoutesList.get(position).getLoad_OK().equals("1")) &&(points_RoutesList.get(position).getOffload_OK().equals("0"))){
				dragAreaLinearLayout.setVisibility(View.VISIBLE);
				TextView leftNameTextView = (TextView) childView.findViewById(R.id.dragNameTextView);
				leftNameTextView.setText(points_RoutesList.get(position).getObj_Name());
				ImageView dragAreaImageView=(ImageView)childView.findViewById(R.id.dragAreaImageView);
				if((points_RoutesList.get(position).getLoad_OK().equals("1")) && ((points_RoutesList.get(position).getOffload_OK().equals("1")) || (points_RoutesList.get(position).getOffload_OK().equals("0"))))
				{
					dragAreaImageView.setPadding(1, 1, 1, 1);
					dragAreaImageView.setBackgroundColor(Color.GREEN);
				}else if((points_RoutesList.get(position).getLoad_OK().equals("-1")) || (points_RoutesList.get(position).getOffload_OK().equals("-1")) ){
					dragAreaImageView.setPadding(1, 1, 1, 1);
					dragAreaImageView.setBackgroundColor(Color.RED);
				}
				leftLinearLayout.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						setExpended(expended, 1);
					}
				});
			}
			moredataLinearLayout.addView(childView);
			if(detail>0)
			{
				View moreChild=layoutInflater.inflate(R.layout.point_route_moredetail, null);
				ImageView closeImageView=(ImageView)moreChild.findViewById(R.id.closeImageView);
				TextView addressTextView=(TextView)moreChild.findViewById(R.id.addressTextView);
				TextView messageTextView=(TextView)moreChild.findViewById(R.id.messageTextView);
				addressTextView.setText(points_RoutesList.get(position).getObj_Address());
				messageTextView.setText(points_RoutesList.get(position).getUser_Message());
				closeImageView.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						setExpended(expended, -1);
						
					}
				});
				detailLinearLayout.addView(moreChild);
			}
			
			
		}else {
			expend_ImageView.setImageResource(R.drawable.expend_close);
		}
		
		point_timeTextView.setText("00:70");
		point_nameTextView.setText(points_RoutesList.get(position).getPoint_Name());
		return view;
	}
	
	private void moveObject(final LinearLayout moveLinearLayout)
	{
		moveLinearLayout.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View view, MotionEvent event) {
//				route_pointListView.setOnTouchListener(new View.OnTouchListener() {
//					
//					@Override
//					public boolean onTouch(View v, MotionEvent listevent) {
//						if(listevent.getAction()==MotionEvent.ACTION_MOVE)
//						{
//							return true;
//						}
//						return false;
//					}
//				});
				RelativeLayout.LayoutParams params = (LayoutParams)view.getLayoutParams();
				switch (event.getAction() & MotionEvent.ACTION_MASK) {
				case MotionEvent.ACTION_DOWN:
					dx = event.getRawX() - params.leftMargin;
					//dy = event.getRawY() - params.topMargin;
					mode = DRAG;
					break;
					
				case MotionEvent.ACTION_UP:
					
					break;
					
				case MotionEvent.ACTION_POINTER_UP:
					mode = NONE;
					break;
					
				case MotionEvent.ACTION_POINTER_DOWN:

					oldDist = spacing(event);
					if (oldDist > 5f) {
						mode = ZOOM;
					}

					break;
				case MotionEvent.ACTION_MOVE:
					if (mode == DRAG) {
						x = event.getRawX();
						y = event.getRawY();
						params.leftMargin = (int) (x-dx);
						//params.topMargin = (int) (y);
						//params.leftMargin = 100;
						//params.topMargin =100;
						moveLinearLayout.setLayoutParams(params);
						Toast.makeText(context, x+"--"+y, Toast.LENGTH_LONG).show();
					}
					break;
				}
				return true;
			}
		});
	}
	
	private float spacing(MotionEvent event) {

		float x = event.getX(0) - event.getX(1);

		float y = event.getY(0) - event.getY(1);

		return FloatMath.sqrt(x * x + y * y);

	}
}

package com.solution24hours.webservice;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

import android.util.Log;

public class WebService {
	private String SOAPRequestXMLHeader = "<?xml version='1.0' encoding='utf-8'?><soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>";
	private String SOAPRequestXMLEnd = "</soap:Envelope>";
	
	public String getPoints_Route(String URL, String user_ID, String route_ID)
	{
		String SOAPRequestXMLBody = " <soap:Body><Points_Route xmlns='http://tempuri.org/'><user_ID>"+user_ID+"</user_ID><route_ID>"+route_ID+"</route_ID></Points_Route></soap:Body>";
		String soapAction = "http://tempuri.org/Points_Route";
		String SOAPRequestXML = SOAPRequestXMLHeader + SOAPRequestXMLBody
				+ SOAPRequestXMLEnd;
		return CallWebService(URL, soapAction, SOAPRequestXML);
	}
	
	public String getRoutes(String URL, String user_ID)
	{
		String SOAPRequestXMLBody = " <soap:Body><Routes xmlns='http://tempuri.org/'><user_ID>"+user_ID+"</user_ID></Routes></soap:Body>";
		String soapAction = "http://tempuri.org/Routes";
		String SOAPRequestXML = SOAPRequestXMLHeader + SOAPRequestXMLBody
				+ SOAPRequestXMLEnd;
		return CallWebService(URL, soapAction, SOAPRequestXML);
	}
	
	public String getContacts(String URL, String user_ID, String route_ID)
	{
		String SOAPRequestXMLBody = " <soap:Body><Contacts xmlns='http://tempuri.org/'><user_ID>"+user_ID+"</user_ID><route_ID>"+route_ID+"</route_ID></Contacts></soap:Body>";
		String soapAction = "http://tempuri.org/Contacts";
		String SOAPRequestXML = SOAPRequestXMLHeader + SOAPRequestXMLBody
				+ SOAPRequestXMLEnd;
		return CallWebService(URL, soapAction, SOAPRequestXML);
	}
	
	public String CallWebService(String url, String soapAction, String envelope) {
		final DefaultHttpClient httpClient = new DefaultHttpClient();
		// request parameters
		HttpParams params = httpClient.getParams();
		HttpConnectionParams.setConnectionTimeout(params, 10000);
		HttpConnectionParams.setSoTimeout(params, 15000);
		// set parameter
		HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), true);

		// POST the envelope
		HttpPost httppost = new HttpPost(url);
		// add headers
		httppost.setHeader("soapaction", soapAction);
		httppost.setHeader("Content-Type", "text/xml; charset=utf-8");

		String responseString = "";
		try {

			// the entity holds the request
			HttpEntity entity = new StringEntity(envelope);
			httppost.setEntity(entity);

			// Response handler
			ResponseHandler<String> rh = new ResponseHandler<String>() {
				// invoked when client receives response
				public String handleResponse(HttpResponse response)
						throws ClientProtocolException, IOException {

					// get response entity
					HttpEntity entity = response.getEntity();

					// read the response as byte array
					StringBuffer out = new StringBuffer();
					byte[] b = EntityUtils.toByteArray(entity);

					// write the response byte array to a string buffer
					out.append(new String(b, 0, b.length));
					return out.toString();
				}
			};

			responseString = httpClient.execute(httppost, rh);

		} catch (Exception e) {
			Log.v("exception", e.toString());
		}

		// close the connection
		httpClient.getConnectionManager().shutdown();
		Log.i("Webservice", responseString);
		return responseString;
	}
}

package com.solution24hours.webservice;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.solution24hours.dal.Contacts;
import com.solution24hours.dal.Points_Route;
import com.solution24hours.dal.Routes;

public class WebServiceParser {
	
	
	public ArrayList<Points_Route> getPoints_Route(String xml,String parent) {
		ArrayList<Points_Route> points_RoutesList= new ArrayList<Points_Route>();
		try {
			NodeList nodeList = getXMLFromString(xml).getElementsByTagName(parent);
			for (int i = 0; i < nodeList.getLength(); i++) {
				Element element = (Element) nodeList.item(i);
				points_RoutesList.add(new Points_Route(getValue(element, "Point_ID"), getValue(element, "Point_Name"), getValue(element, "Route_ID"), getValue(element, "User_Message"), getValue(element, "Notification"), getValue(element, "Point_Coordinates"), getValue(element, "Distance"), getValue(element, "Object_ID"), getValue(element, "Load_OK"), getValue(element, "Load_Time"), getValue(element, "Offload_OK"), getValue(element, "Offload_Time"), getValue(element, "Obj_Name"), getValue(element, "Obj_Description"), getValue(element, "Obj_Address"), getValue(element, "Obj_MSISDN"), getValue(element, "Obj_Comm_Ch_Type"), getValue(element, "Obj_Comm_Ch_Address"), getValue(element, "Obj_Comm_Ch_Gateway"), getValue(element, "User_Name"), getValue(element, "User_MSISDN"), getValue(element, "User_Email"), getValue(element, "Obj_RF_ID"),getValue(element, "Action_ID")));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return points_RoutesList;
	}
	
	public ArrayList<Routes> getRoutes(String xml,String parent)
	{
		ArrayList<Routes> routesList= new ArrayList<Routes>();
		try {
			NodeList nodeList = getXMLFromString(xml).getElementsByTagName(parent);
			for (int i = 0; i < nodeList.getLength(); i++) {
				Element element = (Element) nodeList.item(i);
				routesList.add(new Routes(getValue(element, "user_ID"), getValue(element, "route_ID"), getValue(element, "operation_ID"), getValue(element, "carrier_ID"), getValue(element, "route_Name"), getValue(element, "route_Start_Time"), getValue(element, "activation_Date"), getValue(element, "expiry_Date"), getValue(element, "forced_Expiry_Date"), getValue(element, "start_Coordinates"), getValue(element, "total_Distance"), getValue(element, "total_Interval")));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return routesList;
	}

	public ArrayList<Contacts> getContacts(String xml,String parent)
	{
		ArrayList<Contacts> contactsList= new ArrayList<Contacts>();
		try {
			NodeList nodeList = getXMLFromString(xml).getElementsByTagName(parent);
			for (int i = 0; i < nodeList.getLength(); i++) {
				Element element = (Element) nodeList.item(i);
				contactsList.add(new Contacts(getValue(element, "route_Name"), getValue(element, "object_ID"), getValue(element, "obj_Name"), getValue(element, "obj_MSISDN"), getValue(element, "user_Name"), getValue(element, "user_MSISDN"), getValue(element, "user_Email")));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return contactsList;
	}
	
	private String getValue(Element item, String str) {
		NodeList n = item.getElementsByTagName(str);
		return this.getElementValue(n.item(0));
	}

	private String getElementValue(Node elem) {
		Node child;
		if (elem != null) {
			if (elem.hasChildNodes()) {
				for (child = elem.getFirstChild(); child != null; child = child
						.getNextSibling()) {
					if (child.getNodeType() == Node.TEXT_NODE) {
						return child.getNodeValue();
					}
				}
			}
		}
		return "";
	}

	private Document getXMLFromString(String xml) throws Exception {
		Document doc = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder;
		builder = factory.newDocumentBuilder();
		doc = builder.parse(new InputSource(new StringReader(xml)));
		return doc;

	}
}

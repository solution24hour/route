package com.solution24hours.async_task;

import java.util.ArrayList;

import com.solution24hours.baseadapter.Points_RouteBaseAdapter;
import com.solution24hours.baseurl.BaseUrl;
import com.solution24hours.dal.Points_Route;
import com.solution24hours.webservice.WebService;
import com.solution24hours.webservice.WebServiceParser;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class Points_RouteAsyncTask extends AsyncTask<Void, String, String> {
	private Context context;
	private ListView route_pointListView;
	private ProgressDialog progressDialog;
	private String user_ID;
	private String route_ID;
	private ArrayList<Points_Route> points_RoutesList;
	private Points_RouteBaseAdapter points_RouteBaseAdapter;

	public Points_RouteAsyncTask(Context context, String user_ID,
			String route_ID,ListView route_pointListView) {
		this.context = context;
		this.user_ID = user_ID;
		this.route_ID = route_ID;
		this.route_pointListView=route_pointListView;
		progressDialog= new ProgressDialog(context);
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		progressDialog.setMessage("Please Wait...");
		progressDialog.show();
	}
	@Override
	protected String doInBackground(Void... params) {
		// TODO Auto-generated method stub
		points_RoutesList=new WebServiceParser().getPoints_Route(new WebService().getPoints_Route(BaseUrl.Route_point, user_ID, route_ID), "PointsRouteModel");
		return "DONE";
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if(result.equals("DONE"))
		{
			if(progressDialog.isShowing())
				progressDialog.dismiss();
			if(!points_RoutesList.isEmpty())
			{
				points_RouteBaseAdapter= new Points_RouteBaseAdapter(context, points_RoutesList,route_pointListView);
				route_pointListView.setAdapter(points_RouteBaseAdapter);
				route_pointListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						points_RouteBaseAdapter.setExpended(position,-1);
							
					}
					
				});
				
			}
		}
	}
}

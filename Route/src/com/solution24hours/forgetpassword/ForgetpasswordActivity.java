package com.solution24hours.forgetpassword;

import com.solution24hours.route.R;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class ForgetpasswordActivity extends Activity {

	private EditText userNameEditText;
	private EditText emailEditText;
	private Button sendButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forgetpassword);
		intiailizeViews();
	}

	private void intiailizeViews() {
		userNameEditText = (EditText) findViewById(R.id.userNameEditText);
		emailEditText = (EditText) findViewById(R.id.emailEditText);
		sendButton = (Button) findViewById(R.id.sendButton);
	}
}

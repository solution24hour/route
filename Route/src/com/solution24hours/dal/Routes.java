package com.solution24hours.dal;

public class Routes {
	private String User_ID;
	private String Route_ID;
	private String Operation_ID;
	private String Carrier_ID;
	private String Route_Name;
	private String Route_Start_Time;
	private String Activation_Date;
	private String Expiry_Date;
	private String Forced_Expiry_Date;
	private String Start_Coordinates;
	private String Total_Distance;
	private String Total_Interval;

	public Routes(String user_ID, String route_ID, String operation_ID,
			String carrier_ID, String route_Name, String route_Start_Time,
			String activation_Date, String expiry_Date,
			String forced_Expiry_Date, String start_Coordinates,
			String total_Distance, String total_Interval) {
		super();
		User_ID = user_ID;
		Route_ID = route_ID;
		Operation_ID = operation_ID;
		Carrier_ID = carrier_ID;
		Route_Name = route_Name;
		Route_Start_Time = route_Start_Time;
		Activation_Date = activation_Date;
		Expiry_Date = expiry_Date;
		Forced_Expiry_Date = forced_Expiry_Date;
		Start_Coordinates = start_Coordinates;
		Total_Distance = total_Distance;
		Total_Interval = total_Interval;
	}

	public String getUser_ID() {
		return User_ID;
	}

	public String getRoute_ID() {
		return Route_ID;
	}

	public String getOperation_ID() {
		return Operation_ID;
	}

	public String getCarrier_ID() {
		return Carrier_ID;
	}

	public String getRoute_Name() {
		return Route_Name;
	}

	public String getRoute_Start_Time() {
		return Route_Start_Time;
	}

	public String getActivation_Date() {
		return Activation_Date;
	}

	public String getExpiry_Date() {
		return Expiry_Date;
	}

	public String getForced_Expiry_Date() {
		return Forced_Expiry_Date;
	}

	public String getStart_Coordinates() {
		return Start_Coordinates;
	}

	public String getTotal_Distance() {
		return Total_Distance;
	}

	public String getTotal_Interval() {
		return Total_Interval;
	}

}

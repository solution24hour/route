package com.solution24hours.dal;

import java.util.ArrayList;

public class Points_RouteGroup {
	private String time;
	private String name;
	private ArrayList<Points_Route> points_Routes;

	public Points_RouteGroup(String time, String name,
			ArrayList<Points_Route> points_Routes) {
		super();
		this.time = time;
		this.name = name;
		this.points_Routes = points_Routes;
	}

	public String getTime() {
		return time;
	}

	public String getName() {
		return name;
	}

	public ArrayList<Points_Route> getPoints_Routes() {
		return points_Routes;
	}

}

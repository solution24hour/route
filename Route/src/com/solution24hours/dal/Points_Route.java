package com.solution24hours.dal;

public class Points_Route {

	private String Point_ID;
	private String Point_Name;
	private String Route_ID;
	private String User_Message;
	private String Notification;
	private String Point_Coordinates;
	private String Distance;
	private String Object_ID;
	private String Load_OK;
	private String Load_Time;
	private String Offload_OK;
	private String Offload_Time;
	private String Obj_Name;
	private String Obj_Description;
	private String Obj_Address;
	private String Obj_MSISDN;
	private String Obj_Comm_Ch_Type;
	private String Obj_Comm_Ch_Address;
	private String Obj_Comm_Ch_Gateway;
	private String User_Name;
	private String User_MSISDN;
	private String User_Email;
	private String Obj_RF_ID;
	private String Action_ID;

	public Points_Route(String point_ID, String point_Name, String route_ID,
			String user_Message, String notification, String point_Coordinates,
			String distance, String object_ID, String load_OK,
			String load_Time, String offload_OK, String offload_Time,
			String obj_Name, String obj_Description, String obj_Address,
			String obj_MSISDN, String obj_Comm_Ch_Type,
			String obj_Comm_Ch_Address, String obj_Comm_Ch_Gateway,
			String user_Name, String user_MSISDN, String user_Email,
			String obj_RF_ID, String action_ID) {
		super();
		Point_ID = point_ID;
		Point_Name = point_Name;
		Route_ID = route_ID;
		User_Message = user_Message;
		Notification = notification;
		Point_Coordinates = point_Coordinates;
		Distance = distance;
		Object_ID = object_ID;
		Load_OK = load_OK;
		Load_Time = load_Time;
		Offload_OK = offload_OK;
		Offload_Time = offload_Time;
		Obj_Name = obj_Name;
		Obj_Description = obj_Description;
		Obj_Address = obj_Address;
		Obj_MSISDN = obj_MSISDN;
		Obj_Comm_Ch_Type = obj_Comm_Ch_Type;
		Obj_Comm_Ch_Address = obj_Comm_Ch_Address;
		Obj_Comm_Ch_Gateway = obj_Comm_Ch_Gateway;
		User_Name = user_Name;
		User_MSISDN = user_MSISDN;
		User_Email = user_Email;
		Obj_RF_ID = obj_RF_ID;
		Action_ID = action_ID;
	}

	public String getPoint_ID() {
		return Point_ID;
	}

	public String getPoint_Name() {
		return Point_Name;
	}

	public String getRoute_ID() {
		return Route_ID;
	}

	public String getUser_Message() {
		return User_Message;
	}

	public String getNotification() {
		return Notification;
	}

	public String getPoint_Coordinates() {
		return Point_Coordinates;
	}

	public String getDistance() {
		return Distance;
	}

	public String getObject_ID() {
		return Object_ID;
	}

	public String getLoad_OK() {
		return Load_OK;
	}

	public String getLoad_Time() {
		return Load_Time;
	}

	public String getOffload_OK() {
		return Offload_OK;
	}

	public String getOffload_Time() {
		return Offload_Time;
	}

	public String getObj_Name() {
		return Obj_Name;
	}

	public String getObj_Description() {
		return Obj_Description;
	}

	public String getObj_Address() {
		return Obj_Address;
	}

	public String getObj_MSISDN() {
		return Obj_MSISDN;
	}

	public String getObj_Comm_Ch_Type() {
		return Obj_Comm_Ch_Type;
	}

	public String getObj_Comm_Ch_Address() {
		return Obj_Comm_Ch_Address;
	}

	public String getObj_Comm_Ch_Gateway() {
		return Obj_Comm_Ch_Gateway;
	}

	public String getUser_Name() {
		return User_Name;
	}

	public String getUser_MSISDN() {
		return User_MSISDN;
	}

	public String getUser_Email() {
		return User_Email;
	}

	public String getObj_RF_ID() {
		return Obj_RF_ID;
	}

	public String getAction_ID() {
		return Action_ID;
	}

}

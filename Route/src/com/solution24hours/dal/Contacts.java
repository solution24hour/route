package com.solution24hours.dal;

public class Contacts {
	private String Route_Name;
	private String Object_ID;
	private String Obj_Name;
	private String Obj_MSISDN;
	private String User_Name;
	private String User_MSISDN;
	private String User_Email;

	public Contacts(String route_Name, String object_ID, String obj_Name,
			String obj_MSISDN, String user_Name, String user_MSISDN,
			String user_Email) {
		super();
		Route_Name = route_Name;
		Object_ID = object_ID;
		Obj_Name = obj_Name;
		Obj_MSISDN = obj_MSISDN;
		User_Name = user_Name;
		User_MSISDN = user_MSISDN;
		User_Email = user_Email;
	}

	public String getRoute_Name() {
		return Route_Name;
	}

	public String getObject_ID() {
		return Object_ID;
	}

	public String getObj_Name() {
		return Obj_Name;
	}

	public String getObj_MSISDN() {
		return Obj_MSISDN;
	}

	public String getUser_Name() {
		return User_Name;
	}

	public String getUser_MSISDN() {
		return User_MSISDN;
	}

	public String getUser_Email() {
		return User_Email;
	}

}
